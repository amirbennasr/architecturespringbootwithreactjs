package com.mar.manage.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;

// Responsible for generating tokens specific to a client
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	private static final String CLIEN_ID = "business-client";
	private static final String CLIENT_SECRET = "business-secret";
	
	// Grant types
	private static final String GRANT_TYPE_PASSWORD = "password";
	private static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
	private static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
	private static final String GRANT_TYPE_IMPLICIT = "implicit";
	
	// Scopes
	private static final String SCOPE_READ = "read";
	private static final String SCOPE_WRITE = "write";
	private static final String SCOPE_TRUST = "trust";
	
	// Access token validity and refresh_token validity 
	private static final int ACCESS_TOKEN_VALIDITY_SECONDS = 1*60*60;
	private static final int FREFRESH_TOKEN_VALIDITY_SECONDS = 6*60*60;
    
	//
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final TokenStore tokenStore;
    private final AccessTokenConverter accessTokenConverter;
    
    @Autowired
    public AuthorizationServerConfig(final AuthenticationManager authenticationManager, final TokenStore tokenStore,
            final AccessTokenConverter accessTokenConverter, final PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenStore = tokenStore;
        this.accessTokenConverter = accessTokenConverter;
    }
	
	// Defines the client details service
	@Override
	public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
		configurer.inMemory()
				   .withClient(CLIEN_ID)
				   .secret(passwordEncoder.encode(CLIENT_SECRET))
				   .authorizedGrantTypes(GRANT_TYPE_PASSWORD, GRANT_TYPE_AUTHORIZATION_CODE, GRANT_TYPE_REFRESH_TOKEN, GRANT_TYPE_IMPLICIT ) //  Grant types that are authorized for the client to use. Default value is empty.
				   .scopes(SCOPE_TRUST, SCOPE_READ, SCOPE_WRITE)// The scope to which the client is limited. If scope is undefined or empty (the default) the client is not limited by scope
				   .accessTokenValiditySeconds(ACCESS_TOKEN_VALIDITY_SECONDS)
				   .refreshTokenValiditySeconds(FREFRESH_TOKEN_VALIDITY_SECONDS);
	}
	
	// Defines the authorization and token endpoints and the token services.
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore)
				.authenticationManager(authenticationManager)
                .accessTokenConverter(accessTokenConverter);
	}
	
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()")
          		.checkTokenAccess("isAuthenticated()");
    }
}