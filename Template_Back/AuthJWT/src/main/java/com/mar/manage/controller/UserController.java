package com.mar.manage.controller;

import java.security.Principal;

import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableResourceServer
@RequestMapping("/user")
public class UserController {
    @GetMapping("/current")
    public Principal user(Principal principal) {
        return principal;
    }

}
