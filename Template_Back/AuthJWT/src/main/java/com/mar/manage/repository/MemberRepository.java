package com.mar.manage.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.manage.model.Member;

public interface MemberRepository extends JpaRepository<Member, Long> {
	public Optional<Member> findByLogin(String login);
}
