package com.mar.manage.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mar.manage.model.Member;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
	@Autowired
	private MemberRepository memberRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {		
		Optional<Member> member = memberRepository.findByLogin(login);
        if ( !member.isPresent() ) {
        	throw new UsernameNotFoundException("Invalides login/mot de passe");
        }
        
        return new User(member.get().getLogin(), 
        		member.get().getPassword(), 
        		getAuthority(member.get()));
	}
	
	private List<SimpleGrantedAuthority> getAuthority(Member member) {
        return member.getAuthorities().stream()
        	.map( authority -> new SimpleGrantedAuthority(authority.getAuthority()))
        	.collect(Collectors.toList());
    }
}
