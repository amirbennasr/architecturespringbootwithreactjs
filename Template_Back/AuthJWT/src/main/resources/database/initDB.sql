CREATE TABLE Member(
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
    login varchar(50) NOT NULL,
	password VARCHAR(250) NOT NULL,
	last_name VARCHAR(100) NOT NULL,
	first_name VARCHAR(100) NOT NULL,
    email VARCHAR(250) NOT NULL,
    reference VARCHAR(50) NOT NULL,
);

CREATE TABLE Authority (
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
    authority VARCHAR(100) NOT NULL
);

CREATE TABLE Member_Authority (
  authority_id int NOT NULL,
  member_id int NOT NULL,
  CONSTRAINT member_authority_pk PRIMARY KEY (authority_id, member_id),
  FOREIGN KEY (authority_id) REFERENCES Authority(id),
  FOREIGN KEY (member_id) REFERENCES Member(id)
);

INSERT INTO Authority values(1, 'ADMIN');
INSERT INTO Authority values(2, 'USER');
INSERT INTO Member values(1, 'Admin', '$2a$10$a65XF01UMzCmy6SM.Sz/WOm0htoi/JP8g48xKa2A9wh96ZjGjb2tW', 'admin', 'admin', 'admin@test.com', 'ADMIN');
INSERT INTO Member_Authority values(1, 1);

