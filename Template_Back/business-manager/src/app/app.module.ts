import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

/********** Modules of business-manager*********** */
import { ContainersModule } from './containers/containers.module';
import { ComponentsModule } from './components/components.module';
import { ServicesModule } from './services/services.module';
import { StoreModule } from './store/store.module';

/********** Guards *********** */
import { AuthGuard } from './guards/auth-guard';
import { AdminGuard } from './guards/admin-guard';

const guards = [
  AuthGuard,
  AdminGuard
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    ContainersModule,
    ComponentsModule,
    ServicesModule,
    StoreModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [...guards],
  bootstrap: [AppComponent]
})
export class AppModule { }

