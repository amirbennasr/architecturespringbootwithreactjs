import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { Category } from 'src/app/models/catalog/category';

/*************** Actions and queries *******/
import { CategoriesActions } from '../../../../store/catalog/categories/categories.actions';

/********* Utils ********/
import { CategoryForm } from '../../../../business-forms/catalog/category-form';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss']
})
export class CategoryCreateComponent implements OnInit {
  category: Category;
  form: FormGroup;

  constructor(
    private categoriesActions: CategoriesActions,
    private modelForm: CategoryForm) {
    this.category = new Category();
  }

  ngOnInit() {
    this.form = this.modelForm.createModelForm(this.category);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load category from form
    this.modelForm.formToModel(this.form, this.category);
    this.categoriesActions.saveCategory(this.category);
  }

  onReset() {
    this.form.reset();
  }
}
