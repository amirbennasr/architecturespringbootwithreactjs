import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Actions and queries *******/
import * as categoriesQueries from '../../../../store/catalog/categories/categories.queries';

/********** Models  ************/
import { Category } from '../../../../models/catalog/category';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss']
})

export class CategoryDetailComponent implements OnInit {
  @select(categoriesQueries.category) storeCategory: Observable<Category>;
  category: Category;

  constructor() {}

  ngOnInit() {
    this.storeCategory.subscribe((value) => {
      this.category = value;
    });
  }
}
