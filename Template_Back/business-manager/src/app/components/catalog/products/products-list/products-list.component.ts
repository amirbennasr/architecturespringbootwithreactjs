import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Models *******/
import { Product } from '../../../../models/catalog/product';

/*************** Actions and queries *******/
import { ProductsActions } from '../../../../store/catalog/products/products.actions';
import * as productsQueries from '../../../../store/catalog/products/products.queries';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  @select(productsQueries.getProducts) products: Observable<Product[]>;

  constructor(private productsActions: ProductsActions ) { }

  ngOnInit() {
    this.productsActions.getProducts();
  }

  // redirect to Product detail
  redirectToProductDetail(product: Product) {
    this.productsActions.selectProduct(product);
  }

  // delete a product
  remove(id: number) {
    this.productsActions.removeProduct(id);
  }

}
