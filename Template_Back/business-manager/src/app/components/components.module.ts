import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/********* 'components module' components */
/** ---------- Catalog folder -------- */
import { CategoryDetailComponent } from './catalog/categories/category-detail/category-detail.component';
import { CategoryCreateComponent } from './catalog/categories/category-create/category-create.component';
import { CategoriesListComponent} from './catalog/categories/categories-list/categories-list.component';

import { ProductsListComponent } from './catalog/products/products-list/products-list.component';
import { ProductCreateComponent } from './catalog/products/product-create/product-create.component';
import { ProductDetailComponent } from './catalog/products/product-detail/product-detail.component';

/** ---------- Users folder -------- */
import { UsersListComponent } from './users/users-list/users-list.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';

/** ---------- Profiles folder -------- */
import { BusinessActionCreateComponent } from './profiles/business-actions/business-action-create/business-action-create.component';
import { BusinessActionDetailComponent } from './profiles/business-actions/business-action-detail/business-action-detail.component';
import { BusinessActionsListComponent } from './profiles/business-actions/business-actions-list/business-actions-list.component';

// tslint:disable-next-line: max-line-length
import { BusinessAuthoritiesListComponent } from './profiles/business-authorities/business-authorities-list/business-authorities-list.component';
// tslint:disable-next-line: max-line-length
import { BusinessAuthorityCreateComponent } from './profiles/business-authorities/business-authority-create/business-authority-create.component';
// tslint:disable-next-line: max-line-length
import { BusinessAuthorityDetailComponent } from './profiles/business-authorities/business-authority-detail/business-authority-detail.component';

import { BusinessResourceCreateComponent } from './profiles/business-resources/business-resource-create/business-resource-create.component';
import { BusinessResourceDetailComponent } from './profiles/business-resources/business-resource-detail/business-resource-detail.component';
import { BusinessResourcesListComponent } from './profiles/business-resources/business-resources-list/business-resources-list.component';

const components = [
  CategoryDetailComponent,  CategoryCreateComponent,  CategoriesListComponent,
  ProductsListComponent,  ProductCreateComponent, ProductDetailComponent,
  UsersListComponent, UserCreateComponent, UserDetailComponent,
  BusinessActionCreateComponent, BusinessActionDetailComponent, BusinessActionsListComponent,
  BusinessAuthoritiesListComponent, BusinessAuthorityCreateComponent, BusinessAuthorityDetailComponent,
  BusinessResourceCreateComponent, BusinessResourceDetailComponent, BusinessResourcesListComponent
];

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [ ...components ],
  exports: [ ...components ]
})
export class ComponentsModule { }

