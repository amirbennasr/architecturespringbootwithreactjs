import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { BusinessAction } from '../../../../models/profile/business-action';
import { BusinessResource } from '../../../../models/profile/business-resource';

/*************** Actions and queries *******/
import { BusinessActionsActions } from '../../../../store/profile/business-actions/business-actions.actions';

/**************Services *********/
import { BusinessResourcesService } from '../../../../services/profiles/business-resources/business-resources.service';

/******** Message handlers **********/
import { handleError } from '../../../../utils/message-handler';

/********* Utils ********/
import { BusinessActionForm } from '../../../../business-forms/profile/business-action-form';

@Component({
  selector: 'app-business-action-create',
  templateUrl: './business-action-create.component.html',
  styleUrls: ['./business-action-create.component.scss']
})
export class BusinessActionCreateComponent implements OnInit {
  businessAction: BusinessAction;
  businessResources: BusinessResource[];
  form: FormGroup;

  constructor(
    private businessActionsActions: BusinessActionsActions,
    private businessResourcesService: BusinessResourcesService,
    private modelForm: BusinessActionForm) {
      this.businessAction = new BusinessAction();
  }

  ngOnInit() {
    this.loadListes();
    this.form = this.modelForm.createModelForm(this.businessAction);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load action from form
    this.modelForm.formToModel(this.form, this.businessAction);
    this.businessActionsActions.saveBusinessAction(this.businessAction);
  }

  onReset() {
    this.form.reset();
  }

  loadListes() {
    this.businessResourcesService.getBusinessResources().subscribe(
      (data: BusinessResource[]) => {
        debugger;
        this.businessResources = data; },
      (error) => { handleError(error); }
    );
  }
}
