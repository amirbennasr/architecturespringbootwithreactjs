import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Models *******/
import { BusinessAction } from '../../../../models/profile/business-action';

/*************** Actions and queries *******/
import { BusinessActionsActions } from '../../../../store/profile/business-actions/business-actions.actions';
import * as businessActionsQueries from '../../../../store/profile/business-actions/business-actions.queries';


@Component({
  selector: 'app-business-actions-list',
  templateUrl: './business-actions-list.component.html',
  styleUrls: ['./business-actions-list.component.scss']
})
export class BusinessActionsListComponent implements OnInit {
  @select(businessActionsQueries.getBusinessActions) businessActions: Observable<BusinessAction[]>;

  constructor( private businessActionsActions: BusinessActionsActions ) { }

  ngOnInit() {
    this.businessActionsActions.getBusinessActions();
  }

  // redirect to action detail
  redirectToDetail(businessAction: BusinessAction) {
    debugger;
    this.businessActionsActions.selectBusinessAction(businessAction);
  }

  // delete a action
  remove(id: number) {
    this.businessActionsActions.removeBusinessAction(id);
  }
}
