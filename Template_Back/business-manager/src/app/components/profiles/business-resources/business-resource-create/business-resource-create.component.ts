import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { BusinessResource } from 'src/app/models/profile/business-resource';

/*************** Actions and queries *******/
import { BusinessResourcesActions } from '../../../../store/profile/business-resources/business-resources.actions';

/********* Utils ********/
import { BusinessResourceForm } from '../../../../business-forms/profile/business-resource-form';

@Component({
  selector: 'app-business-resource-create',
  templateUrl: './business-resource-create.component.html',
  styleUrls: ['./business-resource-create.component.scss']
})
export class BusinessResourceCreateComponent implements OnInit {
  businessResource: BusinessResource;
  form: FormGroup;

  constructor(
    private businessResourcesActions: BusinessResourcesActions,
    private modelForm: BusinessResourceForm) {
    this.businessResource = new BusinessResource();
  }

  ngOnInit() {
    this.form = this.modelForm.createModelForm(this.businessResource);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load resource from form
    this.modelForm.formToModel(this.form, this.businessResource);
    this.businessResourcesActions.saveBusinessResource(this.businessResource);
  }

  onReset() {
    this.form.reset();
  }
}
