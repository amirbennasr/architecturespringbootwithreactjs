import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

/*************** Models *******/
import { User } from '../../../models/users/user';
import { BusinessAuthority } from '../../../models/profile/business-authority';

/*************** Actions and queries *******/
import { UsersActions } from '../../../store/users/users.actions';

/**************Services *********/
import { BusinessAuthoritiesService } from '../../../services/profiles/business-authorities/business-authorities.service';

/******** Message handlers **********/
import { handleError } from '../../../utils/message-handler';

/********* Utils ********/
import { UserForm } from '../../../business-forms/user/user-form';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  user: User;
  businessAuthorities: BusinessAuthority[];
  form: FormGroup;

  constructor(
    private usersActions: UsersActions,
    private businessAuthoritiesService: BusinessAuthoritiesService,
    private modelForm: UserForm) {
      this.user = new User();
  }

  ngOnInit() {
    this.loadListes();
    this.form = this.modelForm.createModelForm(this.user);
  }

  onSubmit() {
    if ( this.form.invalid ) {
      return;
    }

    // Load product from form
    this.modelForm.formToModel(this.form, this.user);
    this.usersActions.saveUser(this.user);
  }

  onReset() {
    this.form.reset();
  }

  loadListes() {
    this.businessAuthoritiesService.getBusinessAuthorities().subscribe(
      (data: BusinessAuthority[]) => { this.businessAuthorities = data; },
      (error) => { handleError(error); }
    );
  }
}
