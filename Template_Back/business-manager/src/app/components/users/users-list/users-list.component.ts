import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';

/*************** Models *******/
import { User } from '../../../models/users/user';

/*************** Actions and queries *******/
import { UsersActions } from '../../../store/users/users.actions';
import * as usersQueries from '../../../store/users/users.queries';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @select(usersQueries.getUsers) users: Observable<User[]>;

  constructor(private usersActions: UsersActions ) { }

  ngOnInit() {
    this.usersActions.getUsers();
  }

  // redirect to User detail
  redirectToUserDetail(user: User) {
    this.usersActions.selectUser(user);
  }

  // delete a user
  remove(id: number) {
    this.usersActions.removeUser(id);
  }

}
