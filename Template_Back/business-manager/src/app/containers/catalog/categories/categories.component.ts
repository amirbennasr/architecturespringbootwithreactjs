import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
/************* App state ********/
import { AuthActions } from '../../../services/auth/auth.actions';
import { CategoriesActions } from '../../../store/catalog/categories/categories.actions';
import * as categoriesQueries from '../../../store/catalog/categories/categories.queries';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @select(categoriesQueries.action) appAction: Observable<string>;
  action: string;

  constructor( private categoriesActions: CategoriesActions, private authActions: AuthActions ) {}

  ngOnInit() {
    this.appAction.subscribe((value) => {
      this.action = value;
    });
  }

  // redirect to create new Category
  redirectToCreateCategory() {
    this.categoriesActions.redirectToCreateCategory();
  }

  // Redirect to categories list
  redirectToCategoriesList() {
    this.categoriesActions.redirectToCategoriesList();
  }
}
