import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
/************* App state ********/
import { ProductsActions } from '../../../store/catalog/products/products.actions';
import * as productsQueries from '../../../store/catalog/products/products.queries';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @select(productsQueries.action) appAction: Observable<string>;
  action: string;

  constructor( private productsActions: ProductsActions ) {}

  ngOnInit() {
    this.appAction.subscribe((value) => {
      this.action = value;
    });
  }

  // redirect to create new Product
  redirectToCreateProduct() {
    this.productsActions.redirectToCreateProduct();
  }

  // Redirect to Products list
  redirectToProductsList() {
    this.productsActions.redirectToProductsList();
  }
}
