import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/********* Components modules  *******/
import { ComponentsModule } from '../components/components.module';

/********* components module components */
/** ---------- Catalog folder -------- */
import { CategoriesComponent } from './catalog/categories/categories.component';
import { ProductsComponent } from './catalog/products/products.component';
/** ---------- Auther folder -------- */
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ForbiddenPageComponent } from './forbidden-page/forbidden-page.component';
/** ---------- Common folder -------- */
import { HeaderComponent } from './common/header/header.component';
import { MenuComponent } from './common/menu/menu.component';
/** ---------- User folder -------- */
import { UsersComponent } from './users/users.component';
/** ---------- Profile folder -------- */
import { BusinessAuthoritiesComponent } from './profile/business-authorities/business-authorities.component';
import { BusinessActionsComponent } from './profile/business-actions/business-actions.component';
import { BusinessResourcesComponent } from './profile/business-resources/business-resources.component';


const components = [
  CategoriesComponent,
  ProductsComponent,
  PageNotFoundComponent,
  HomeComponent,
  UsersComponent,
  HeaderComponent,
  MenuComponent,
  LoginComponent,
  ForbiddenPageComponent,
  BusinessActionsComponent,
  BusinessAuthoritiesComponent,
  BusinessResourcesComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [ ...components ],
  exports: [ ...components ]
})
export class ContainersModule { }
