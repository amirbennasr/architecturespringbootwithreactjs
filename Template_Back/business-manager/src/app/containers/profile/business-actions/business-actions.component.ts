import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
/************* App state ********/
import { BusinessActionsActions } from '../../../store/profile/business-actions/business-actions.actions';
import * as buinessActionsQueries from '../../../store/profile/business-actions/business-actions.queries';


@Component({
  selector: 'app-business-actions',
  templateUrl: './business-actions.component.html',
  styleUrls: ['./business-actions.component.scss']
})
export class BusinessActionsComponent implements OnInit {
  @select(buinessActionsQueries.action) appAction: Observable<string>;
  action: string;

  constructor( private businessActionsActions: BusinessActionsActions ) { }

  ngOnInit() {
    this.appAction.subscribe((value) => {
      this.action = value;
    });
  }

  // redirect to create new Action
  redirectToCreate() {
    this.businessActionsActions.redirectToCreateBusinessAction();
  }

  // Redirect to actions list
  redirectToList() {
    this.businessActionsActions.redirectToBusinessActionsList();
  }
}
