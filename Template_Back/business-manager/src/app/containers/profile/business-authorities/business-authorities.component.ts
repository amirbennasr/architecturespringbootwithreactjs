import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
/************* App state ********/
import { BusinessAuthoritiesActions } from '../../../store/profile/business-authorities/business-authorities.actions';
import * as businessAuthoritiesQueries from '../../../store/profile/business-authorities/business-authorities.queries';


@Component({
  selector: 'app-business-authorities',
  templateUrl: './business-authorities.component.html',
  styleUrls: ['./business-authorities.component.scss']
})
export class BusinessAuthoritiesComponent implements OnInit {
  @select(businessAuthoritiesQueries.action) appAction: Observable<string>;
  action: string;

  constructor( private businessAuthoritiesActions: BusinessAuthoritiesActions ) { }

  ngOnInit() {
    this.appAction.subscribe((value) => {
      this.action = value;
    });
  }

  // redirect to create new Authority
  redirectToCreate() {
    this.businessAuthoritiesActions.redirectToCreateBusinessAuthority();
  }

  // Redirect to authorities list
  redirectToList() {
    this.businessAuthoritiesActions.redirectToBusinessAuthoritiesList();
    }
}
