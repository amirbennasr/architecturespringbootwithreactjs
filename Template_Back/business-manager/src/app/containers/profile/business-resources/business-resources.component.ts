import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
/************* App state ********/
import { BusinessResourcesActions } from '../../../store/profile/business-resources/business-resources.actions';
import * as businessResourcesQueries from '../../../store/profile/business-resources/business-resources.queries';


@Component({
  selector: 'app-business-resources',
  templateUrl: './business-resources.component.html',
  styleUrls: ['./business-resources.component.scss']
})
export class BusinessResourcesComponent implements OnInit {
  @select(businessResourcesQueries.action) appAction: Observable<string>;
  action: string;

  constructor( private businessResourcesActions: BusinessResourcesActions ) { }

  ngOnInit() {
    this.appAction.subscribe((value) => {
      this.action = value;
    });
  }

  // redirect to create new resource
  redirectToCreate() {
    this.businessResourcesActions.redirectToCreateBusinessResource();
  }

  // Redirect to resources list
  redirectToList() {
    this.businessResourcesActions.redirectToBusinessResourcesList();
  }
}
