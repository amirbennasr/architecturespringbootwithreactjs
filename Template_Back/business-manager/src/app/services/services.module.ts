import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

/******* Catalog ****/
import { ProductsService } from './catalog/products/products.service';
import { CategoriesService } from './catalog/categories/categories.service';
/******* User ****/
import { UsersService } from './users/users.service';
/******* Auth ****/
import { AuthService } from './auth/auth.service';
/******* Profile ****/
import { BusinessAuthoritiesService } from './profiles/business-authorities/business-authorities.service';
import { BusinessActionsService } from './profiles/business-actions/business-actions.service';
import { BusinessResourcesService } from './profiles/business-resources/business-resources.service';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from '../interceptors/http-error-interceptor';
import { JwtInterceptor } from '../interceptors/jwt-interceptor';

const providers = [
  CategoriesService,
  ProductsService,
  UsersService,
  AuthService,
  BusinessAuthoritiesService,
  BusinessActionsService,
  BusinessResourcesService
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ...providers,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }/*,
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true }*/
  ]
})
export class ServicesModule { }
