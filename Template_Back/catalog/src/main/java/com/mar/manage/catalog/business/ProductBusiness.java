package com.mar.manage.catalog.business;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;
import javax.validation.Valid;

import com.mar.manage.catalog.dto.MapperDTO;
import com.mar.manage.catalog.dto.ProductDTO;
import com.mar.manage.catalog.exception.CategoryNotFoundException;
import com.mar.manage.catalog.exception.ModelNotValideException;
import com.mar.manage.catalog.exception.ProductNotFoundException;
import com.mar.manage.catalog.model.Product;
import com.mar.manage.catalog.repository.CategoryRepository;
import com.mar.manage.catalog.repository.ProductRepository;

@Component
public class ProductBusiness {
	
	protected ProductRepository productRepository;
	protected CategoryRepository categoryRepository;
	protected MapperDTO mapperDTO;
	Validator validator;
	
	@Autowired
	public ProductBusiness(MapperDTO mapperDTO,
			ProductRepository productRepository,
			CategoryRepository categoryRepository,
			Validator validator) {
		this.mapperDTO = mapperDTO;
		this.productRepository = productRepository;
		this.categoryRepository = categoryRepository;
		this.validator = validator;
	}
	
	public Product buildNewProduct(ProductDTO productDTO) 
			throws ModelNotValideException {
		
		// Build product from productDTO
		Product product = mapperDTO.convertToEntity(productDTO);
		
		// Set id, createDate and lastUpdatedDate
		product.setId(0);
		product.setCreateDate(new Date());
		product.setLastUpdateDate(null);
		
		// Validate the product
		if(!isValide(product)) {
			throw new ModelNotValideException();
		}
		
		return product;
	}
	
	public Product buildExistedProduct(ProductDTO productDTO) 
			throws ProductNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !productRepository.existsById(productDTO.getId()) ) {
			throw new CategoryNotFoundException(Long.toString(productDTO.getId()));
		}
				
		// Build product from productDTO
		Product product = mapperDTO.convertToEntity(productDTO);
		
		// Set createDate and lastUpdateDate
		product.setLastUpdateDate(new Date());
		
		// Validate the product
		if(!isValide(product)) {
			throw new ModelNotValideException();
		}
		
		return product;
	}
	
	// Validate the product data
	public boolean isValide(@Valid Product product) {
		//violations = validator.validate(product);
		return true;
	}
}
