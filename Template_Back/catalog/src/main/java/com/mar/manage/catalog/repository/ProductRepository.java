package com.mar.manage.catalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

import com.mar.manage.catalog.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	@Query("SELECT count(*) from Product")
	public int countProducts();
	
	public Optional<Product> findByName(String name);

}
