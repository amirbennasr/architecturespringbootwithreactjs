package com.mar.manage.proxy.config;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableOAuth2Sso
@EnableWebSecurity(debug=true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static final String OAUTH_PATH = "/oauth/**";
	private static final String LOGIN_PATH = "/login";
	private static final String ERROR_PATH = "/error";
	
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    // Configure paths  
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.logout().permitAll()
        	.and().csrf().disable()
//	        .anonymous().disable()
	        .authorizeRequests()
	        .antMatchers(OAUTH_PATH, LOGIN_PATH, ERROR_PATH).permitAll()
	        .anyRequest().permitAll()
	        .and().exceptionHandling()
	        .accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}
