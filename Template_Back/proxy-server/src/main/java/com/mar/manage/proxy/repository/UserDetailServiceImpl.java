package com.mar.manage.proxy.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mar.manage.proxy.model.Authority;
import com.mar.manage.proxy.model.Member;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
	@Autowired
	private MemberRepository memberRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {		
		Optional<Member> member = memberRepository.findByLogin(login);
        if ( !member.isPresent() ) {
        	throw new UsernameNotFoundException("Invalides login/mot de passe");
        }

        return new User(member.get().getLogin(), 
        		member.get().getPassword(), 
        		getAuthority(member.get()));
	}
	
	private List<SimpleGrantedAuthority> getAuthority(Member member) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        for(Authority authority : member.getAuthorities()) {
        	authorities.add(
        			new SimpleGrantedAuthority(
        					authority.getAuthority()));
        }
    	return authorities;
    }
}
