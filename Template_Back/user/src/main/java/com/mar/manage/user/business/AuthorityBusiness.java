package com.mar.manage.user.business;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mar.manage.user.dto.AuthorityDTO;
import com.mar.manage.user.dto.MapperDTO;
import com.mar.manage.user.exception.AuthorityNotFoundException;
import com.mar.manage.user.exception.ModelNotValideException;
import com.mar.manage.user.model.Authority;
import com.mar.manage.user.repository.AuthorityRepository;

@Component
public class AuthorityBusiness {
	
	protected AuthorityRepository authorityRepository;
	protected MapperDTO mapperDTO;
	
	@Autowired
	public AuthorityBusiness(MapperDTO mapperDTO,
			AuthorityRepository authorityRepository) {
		this.mapperDTO = mapperDTO;
		this.authorityRepository = authorityRepository;
	}
	
	public Authority buildNewAuthority(AuthorityDTO authorityDTO) 
			throws ModelNotValideException {
		
		Authority authority = mapperDTO.convertToEntity(authorityDTO);
		
		// Set id, createDate and lastUpdatedDate
		authority.setId(0);
		// TODO: to activate when created date is added to authority
//		authority.setCreateDate(new Date());
		
		// Validate the authority
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return authority;
	}
	
	public Authority buildExistedAuthority(AuthorityDTO authorityDTO) 
			throws AuthorityNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !authorityRepository.existsById(authorityDTO.getId()) ) {
			throw new AuthorityNotFoundException(Long.toString(authorityDTO.getId()));
		}
				
		// Build product from productDTO
		Authority authority = mapperDTO.convertToEntity(authorityDTO);
				
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
		
		return authority;
	}
	
	// Validate the authority data
	public boolean isValide() {
		return true;
	}
}
