package com.mar.manage.user.business;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.mar.manage.user.dto.MapperDTO;
import com.mar.manage.user.exception.MemberNotFoundException;
import com.mar.manage.user.exception.ModelNotValideException;
import com.mar.manage.user.model.Member;
import com.mar.manage.user.repository.MemberRepository;

@Component
public class MemberBusiness {
	
	protected MemberRepository memberRepository;
	protected MapperDTO mapperDTO;
	
	@Autowired
	public MemberBusiness(MapperDTO mapperDTO,
			MemberRepository memberRepository) {
		this.mapperDTO = mapperDTO;
		this.memberRepository = memberRepository;
	}
	
	// Create new member from member
	public void buildNewMember(Member member) 
			throws ModelNotValideException {
		// Set id, createDate and lastUpdatedDate
		member.setId(0);
		
		// Set password
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		member.setPassword(encoder.encode(member.getPassword()));

		// TODO: to activate when created date is added to member 
//		member.setCreateDate(new Date());
		
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
	}
	
	// Create existed member from memberDTO
	public void buildExistedMember(Member member) 
			throws MemberNotFoundException, ModelNotValideException {
		
		// Check the existence of the object
		if( !memberRepository.existsById(member.getId()) ) {
			throw new MemberNotFoundException(Long.toString(member.getId()));
		}
		
		// If password is not empty then the user change it
		if( Objects.isNull(member.getPassword()) && !member.getPassword().isEmpty() ) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			member.setPassword(encoder.encode(member.getPassword()));
		}
		
		// Validate the product
		if(!isValide()) {
			throw new ModelNotValideException();
		}
	}
	
	// Validate the product data
	public boolean isValide() {
		return true;
	}
}
