package com.mar.manage.user.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "user-service";
	private static final String USER_AUTHORITY = "USER";
	private static final String ADMIN_AUTHORITY = "ADMIN";
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
	        .antMatchers("/members/username/**").hasAnyAuthority(ADMIN_AUTHORITY, USER_AUTHORITY)
	        .anyRequest().hasAuthority(ADMIN_AUTHORITY)
	        .and().exceptionHandling()
	        .accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}
}