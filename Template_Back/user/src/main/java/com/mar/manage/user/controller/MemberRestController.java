package com.mar.manage.user.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.user.exception.ModelNotValideException;
import com.mar.manage.user.business.MemberBusiness;
import com.mar.manage.user.dto.MemberDTO;
import com.mar.manage.user.dto.MapperDTO;
import com.mar.manage.user.exception.MemberNotFoundException;
import com.mar.manage.user.model.Member;
import com.mar.manage.user.repository.MemberRepository;

@RestController
@RequestMapping("/members")
public class MemberRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MemberRestController.class);
	protected MemberRepository memberRepository;
	protected MapperDTO mapperDTO;
	protected MemberBusiness memberBusiness;
	
	@Autowired
	public MemberRestController(MemberRepository memberRepository,
			MapperDTO mapperDTO,
			MemberBusiness memberBusiness) {
		
		LOGGER.info("MemberRestController cosntructor has been called");
		this.memberRepository = memberRepository;
		this.mapperDTO = mapperDTO;
		this.memberBusiness = memberBusiness; 
	}

	/**
	 * @param name
	 * @return categories with name = Name
	 */
	// Get entity by name
	@GetMapping("/username/{login}")
	public ResponseEntity<MemberDTO> getMemberByLogin(@PathVariable("login") String login) 
			throws MemberNotFoundException {
		
		LOGGER.info("Member-service byUserLogin() " + login);

		Optional<Member> member = memberRepository.findByLogin(login);
		
		if( !member.isPresent() ) {
			throw new MemberNotFoundException(login);
		}
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(member.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param id
	 * @return member
	 */
	// Get entity by id
	@GetMapping("/{id}")
	public ResponseEntity<MemberDTO> getMemberById(@PathVariable("id") long id) 
			throws MemberNotFoundException {
		
		LOGGER.info("Members-service byId() " + id);

		Optional<Member> member = memberRepository.findById(id);
		
		if( !member.isPresent() ) {
			throw new MemberNotFoundException(Long.toString(id));
		}
		
		return new ResponseEntity<>(
				mapperDTO.convertToDTO(member.get()),
				HttpStatus.OK);
	}
	
	/**
	 * @param
	 * @return all members
	 */
	// Get entities
	@GetMapping("/")
	public ResponseEntity<List<MemberDTO>> getAllMembers() {
		List<Member> result = memberRepository.findAll();
		
		List<MemberDTO> resultDTO = result.stream()
		          .map(item -> mapperDTO.convertToDTO(item))
		          .collect(Collectors.toList());
		
		return new ResponseEntity<>(
				resultDTO,
				HttpStatus.OK);
	}
	
	/**
	 * @param memberDTO
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	// Create entity
	@PostMapping("create")
	public ResponseEntity<Member> saveMember(@Valid @RequestBody Member member ) 
			throws MethodArgumentNotValidException, ModelNotValideException  {
		
		LOGGER.info("members-service Save");
		
		// Create member from memberDTO 
		memberBusiness.buildNewMember(member);
		
		memberRepository.save(member);
		
		return new ResponseEntity<>(
				member,
				HttpStatus.OK);
	}
	
	/**
	 * @param memberDTO
	 * @return HTTP Code
	 * @throws ModelNotValideException 
	 */
	// Update entity
	@PutMapping("update")
	public ResponseEntity<Member> updateMember(@Valid @RequestBody Member member) 
			throws MemberNotFoundException, MethodArgumentNotValidException, ModelNotValideException {
		
		LOGGER.info("Categories-service Save");
		
		memberRepository.save(member);
		
		return new ResponseEntity<>(
				member,
				HttpStatus.OK);
	}
	
	// Delete entity
	@DeleteMapping("delete/{id}")
	public ResponseEntity<Long> deleteEntity(@PathVariable long id) throws MemberNotFoundException {
		
		if ( !memberRepository.existsById(id) ) {
			throw new MemberNotFoundException((new Long(id)).toString());
		}
		
		memberRepository.deleteById(id);
		return new ResponseEntity<>(
				new Long(id),
				HttpStatus.OK);
	}
}
