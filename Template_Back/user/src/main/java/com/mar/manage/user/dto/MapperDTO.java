package com.mar.manage.user.dto;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Component;

import com.mar.manage.user.model.Authority;
import com.mar.manage.user.model.Member;

@Component
public final class MapperDTO {
	private ModelMapper modelMapper;;
	
	@Autowired
	public MapperDTO(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}
	
	/********* Member Mapper ************/
	public AuthorityDTO convertToDTO(Authority entity) {
		return modelMapper.map(entity, AuthorityDTO.class);
	}
	
	public Authority convertToEntity(AuthorityDTO dto) throws ParseException {
		// TODO : create date as catalog
		return modelMapper.map(dto, Authority.class);
	}
	
	/********* Member Mapper ************/
	public MemberDTO convertToDTO(Member entity) {
		return modelMapper.map(entity, MemberDTO.class);
	}
	
	public Member convertToEntity(MemberDTO dto) throws ParseException {
		// TODO : create date as catalog
		return modelMapper.map(dto, Member.class);
	}
}
