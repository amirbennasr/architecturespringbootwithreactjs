export const roles = {
  dashboard: ['ADMIN'],
  categories_list : ['ROLE_GET_CATEGORIES'],
  create_category : ['ROLE_CREATE_CATEGORY'],
  update_category : ['ROLE_UPDATE_CATEGORY'],
  get_category : ['ROLE_GET_CATEGORY'],
  delete_category : ['ROLE_DELETE_CATEGORY']
}