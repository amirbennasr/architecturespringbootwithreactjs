import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { setUserData } from "../redux/actions/UserActions";
import jwtAuthService from "../services/jwtAuthService";
import history from "history.js";
import {LOCALSTORAGE} from '../configuration/AuthConfig';
import {LOCAL_PATHS} from '../configuration/LocalPaths';

class Auth extends Component {
  
  constructor(props) {
    super(props);
    this.props.setUserData(
      JSON.parse(localStorage.getItem(LOCALSTORAGE.USER_ITEM)));
    this.checkJwtAuth();
  }

  checkJwtAuth = () => {
    jwtAuthService.loginWithToken().then(user => {
      this.props.setUserData(user);
    }).catch(err => {
      history.push({
        pathname: LOCAL_PATHS.SINGIN
      });
    });
  };

  render() {
    const { children } = this.props;
    return <Fragment>{children}</Fragment>;
  }
}

const mapStateToProps = state => ({
  setUserData: PropTypes.func.isRequired,
  login: state.login
});

export default connect(
  mapStateToProps,
  { setUserData }
)(Auth);
