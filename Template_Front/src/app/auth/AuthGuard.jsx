import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import AppContext from "app/appContext";
import {LOCAL_PATHS} from '../configuration/LocalPaths';

class AuthGuard extends Component {
  constructor(props, context) {
    super(props);
    let { routes } = context;

    this.state = {
      hasRight: false,
      routes
    };
  }

  componentDidUpdate() {
    if (!this.state.hasRight) {
      this.redirectRoute(this.props);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.hasRight !== this.state.hasRight;
  }

  static getDerivedStateFromProps(props, state) {
    const { location, user } = props;
    const { pathname } = location;
    const matched = state.routes.find(r => r.path === pathname);
    const hasRight =
      matched && matched.auth && matched.auth.length
      && user && user.authorities
        ? matched.auth.filter(value => user.authorities.includes(value)).length > 0
        : true;

    return {
      hasRight
    };
  }

  redirectRoute(props) {
    const { location, history } = props;
    const { pathname } = location;

    history.push({
      pathname: LOCAL_PATHS.NO_RIGHT,
      state: { redirectUrl: pathname }
    });
  }

  render() {
    let { children } = this.props;
    const { hasRight } = this.state;
    return hasRight ? <Fragment>{children}</Fragment> : null;
  }
}

AuthGuard.contextType = AppContext;

const mapStateToProps = state => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps)(AuthGuard));
