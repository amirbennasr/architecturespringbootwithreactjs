const CREDENTIALS = {
    client_id: 'business-client',
    client_secret: 'business-secret',
    grant_type: 'password',
    scope: 'trust'
};

const OPTIONS = {
    headers: {
      'Authorization': 'Basic ' + btoa(CREDENTIALS.client_id + ':' + CREDENTIALS.client_secret),
      'Content-type': 'application/x-www-form-urlencoded'
    }
};

const LOCALSTORAGE = {
    TOKEN_ITEM: 'jwt_token', 
    USER_ITEM: 'auth_user'
};

export {
    CREDENTIALS,
    OPTIONS,
    LOCALSTORAGE
};

