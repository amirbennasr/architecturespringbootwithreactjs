const AUTH_END_POINTS = {
    LOGIN: 'http://localhost:9990/oauth/oauth/token',
    LOGOUT: '',
    CHECK_TOKEN: 'http://localhost:9990/oauth/oauth/check_token'
}

const USER_END_POINTS = {
    GET_BY_LOGIN : 'http://localhost:9990/user/members/username/'
}

const CATEGORY_ENDPOINTS = {
    GET_ALL : 'http://localhost:9990/catalog/categories/',
    CREATE_CATEGORY : 'http://localhost:9990/catalog/categories/create',
    DELETE_CATEGORY : 'http://localhost:9990/catalog/categories/delete/',
    UPDATE_CATEGORY : 'http://localhost:9990/catalog/categories/update',
    GET_CATEGORY : 'http://localhost:9990/catalog/categories/'
}

export {
    USER_END_POINTS,
    AUTH_END_POINTS,
    CATEGORY_ENDPOINTS
};