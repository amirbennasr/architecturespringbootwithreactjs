const LOCAL_PATHS = {
    HOME: '/',
    SINGIN: '/session/signin',
    SING_UP: '/session/signup',
    FORGET_PASSWORD: '/session/forgot-password',
    NOT_FOUND: '/session/404',
    NO_RIGHT: '/session/403'
}

const CATEGORIES_LOCAL_PATHS = {
    LIST : '/categories/list'
}

export {
    LOCAL_PATHS,
    CATEGORIES_LOCAL_PATHS
};