export default class User {
    constructor(userId, login, token, authorities) {
        this.userId = userId;
        this.login = login;
        this.token = token;
        this.authorities = authorities
    }  
};