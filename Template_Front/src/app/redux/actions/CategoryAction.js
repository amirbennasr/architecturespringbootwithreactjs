import categoryService from "../../services/categoriesService";
import history from "history.js";

import {CATEGORIES_LOCAL_PATHS} from '../../configuration/LocalPaths';

export const GET_CATEGORIES = "GET_CATEGORIES";
export const DELETE_CATEGORY = "DELETE_CATEGORY";
export const CREATE_CATEGORY = "CREATE_CATEGORY";
export const UPDATE_CATEGORY = "UPDATE_CATEGORY";
export const CATEGORIES_ERROR = "CATEGORIES_ERROR";
export const GET_CATEGORY = "GET_CATEGORY";

export function getCategories() {
    debugger
    return dispatch => {
        categoryService.getCategories()
            .then(response => {
                return dispatch({
                    type: GET_CATEGORIES,
                    categoryList: response 
                });
            })
            
            .catch(error => {
                return dispatch({
                    type: CATEGORIES_ERROR,
                    data: error
                });
            });
    };
}

export function createCategory(category) {
    debugger;
    return dispatch => {
        debugger;
        categoryService.createCategory(category)
            .then(response => {
                debugger;
                history.push({
                    pathname: CATEGORIES_LOCAL_PATHS.LIST
                });

                return dispatch({
                    type: CREATE_CATEGORY,
                    category: response 
                });
            })
            
            .catch(error => {
                debugger;
                return dispatch({
                    type: CATEGORIES_ERROR,
                    data: error
                });
            });
    };
}

export function updateCategory(category) {
    debugger;
    return dispatch => {
        debugger;
        categoryService.updateCategory(category)
            .then(response => {
                return dispatch({
                    type: UPDATE_CATEGORY,
                    category: response 
                });
            })
            
            .catch(error => {
                debugger;
                return dispatch({
                    type: CATEGORIES_ERROR,
                    data: error
                });
            });
    };
}

export function deleteCategory(id) {
    debugger
    return dispatch => {
        debugger;
        categoryService.deleteCategory(id)
            .then(response => {
                return dispatch({
                    type: DELETE_CATEGORY,
                    id: response 
                });
            })
            
            .catch(error => {
                debugger;
                return dispatch({
                    type: CATEGORIES_ERROR,
                    data: error
                });
            });
    };
}

export function getCategory(id) {
    debugger
    return dispatch => {
        categoryService.getCategory(id)
            .then(response => {
                debugger;
                return dispatch({
                    type: GET_CATEGORY,
                    category: response 
                });
            })
            
            .catch(error => {
                debugger;
                return dispatch({
                    type: CATEGORIES_ERROR,
                    data: error
                });
            });
    };
}