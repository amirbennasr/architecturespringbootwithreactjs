import Category from '../../models/category';

import {
    GET_CATEGORIES,
    DELETE_CATEGORY, 
    CREATE_CATEGORY,
    UPDATE_CATEGORY,
    GET_CATEGORY,
    CATEGORIES_ERROR
  } from "../actions/CategoryAction";

  const initialState = {
    categoryList: [{
      name:'',
      description:''
    }],
    category: new Category(0, '',''),
  };
  
  const CategoryReducer = function(state = initialState, action) {
     switch (action.type) {
      case GET_CATEGORIES: {
        return {
          ...state,
          categoryList: [...action.categoryList],
          category: state.category
        };
      }

      case DELETE_CATEGORY: {
        const post_id = action.id
        return {
          categoryList: state.categoryList.filter(post => post.id !== post_id)
        }
      }

      case CREATE_CATEGORY: {
        const newArray = [...state.categoryList]; //Copying state array
        newArray.splice(2, 0, action.category);
        //using splice to insert at an index
       return {
        ...state,
        categoryList: newArray //reassigning todos array to new array
        }
       }
 
    case UPDATE_CATEGORY: {
      debugger
      const { id, ...rest } = action.category
  
      return {
        categoryList: state.categoryList.map(post => {
          if (post.id === id) {
            return { ...post, ...rest }
          }
          return post
        })
      }
    }
      case GET_CATEGORY: {
        debugger;
        return {
          ...state,
          categoryList: [...state.categoryList],
          category: action.category
        };
      }

      case CATEGORIES_ERROR: {
        debugger;
        return {
          success: false,
          error: action.data
        };
      }
      
      default: {
        return {
          ...state
        };
      }
    }
  };
  
  export default CategoryReducer;
  