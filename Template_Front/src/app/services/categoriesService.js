import axios from "axios";
import {CATEGORY_ENDPOINTS} from '../configuration/EndPoints';

class CategoriesService {
  getCategories=()=>{
    return new Promise((resolve, reject) => {
      axios.get(CATEGORY_ENDPOINTS.GET_ALL)
        .then(response => {    
          resolve(response.data);
          debugger
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  createCategory = (category) => {
    debugger;
    return new Promise((resolve, reject) => {
      axios.post(CATEGORY_ENDPOINTS.CREATE_CATEGORY, category)
        .then(response => {    
          debugger;
          resolve(response.data);
        })
        .catch(error => {
          debugger;
          reject(error)
        })
    })
  }

  deleteCategory = (id) => {
    debugger;
    return new Promise((resolve, reject) => {
      axios.delete(CATEGORY_ENDPOINTS.DELETE_CATEGORY + id)
        .then(response => {    
          debugger;
          resolve(response.data);
        })
        .catch(error => {
          debugger;
          reject(error)
        })
    })
  }

  updateCategory = (category) => {
    debugger;
    return new Promise((resolve, reject) => {
      axios.put(CATEGORY_ENDPOINTS.UPDATE_CATEGORY, category)
        .then(response => {    
          debugger;
          resolve(response.data);
        })
        .catch(error => {
          debugger;
          reject(error)
        })
    })
  }

  getCategory = (id) => {
    debugger;
    return new Promise((resolve, reject) => {
      axios.get(CATEGORY_ENDPOINTS.GET_CATEGORY + id)
        .then(response => {    
          debugger;
          resolve(response.data);
        })
        .catch(error => {
          debugger;
          reject(error)
        })
    })
  }
}

export default new CategoriesService();
