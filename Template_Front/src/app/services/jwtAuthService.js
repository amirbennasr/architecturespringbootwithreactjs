import axios from "axios";
import localStorageService from "./localStorageService";
import qs from 'qs';
import User from '../models/user.js';
import {CREDENTIALS, OPTIONS, LOCALSTORAGE} from '../configuration/AuthConfig';
import {AUTH_END_POINTS, USER_END_POINTS} from '../configuration/EndPoints';

class JwtAuthService {
  getUserByLogin=(login)=>{
    return new Promise((resolve, reject) => {
      axios.get(USER_END_POINTS.GET_BY_LOGIN + login)
        .then(response => {    
          let user = new User(response.data.id,
            response.data.login, 
            localStorage.getItem(LOCALSTORAGE.TOKEN_ITEM), 
            response.data.authorities.map((d) => d.authority));

          this.setUser(user);
          resolve(user);
        })
        .catch(error => {
          reject(error)
        })
    })
  }
   
  loginWithEmailAndPassword = (username,password) => {
    let data = qs.stringify({
      username: username,
      password: password,
      grant_type: CREDENTIALS.grant_type,
      
    });

    return new Promise((resolve, reject) => {
      axios.post(AUTH_END_POINTS.LOGIN, data, OPTIONS)
        .then(response => {
          this.setTokenSession(response.data.access_token);
          resolve(this.getUserByLogin(username));
        })
        .catch(error =>{
          reject(error);
        })
    })
  }

  // Check token is valid
  // This method is being used when user logged in & app is reloaded
  loginWithToken = () => {
    let data = qs.stringify({
      token: localStorage.getItem(LOCALSTORAGE.TOKEN_ITEM)
    });
    
    return new Promise((resolve, reject) => {
      axios.post(AUTH_END_POINTS.CHECK_TOKEN, data)
        .then(response => {
          if (this.checkUserInLocalStorage(response.data)) {
            this.setDefaultRequestsHeader();
            resolve(JSON.parse(localStorage.getItem(LOCALSTORAGE.USER_ITEM)))
          } 
          
          reject("Unknown user");
        })
        .catch(error =>{
          reject(error);
        })
    });
  };

  checkUserInLocalStorage = data => {
    let current = localStorage.getItem(LOCALSTORAGE.USER_ITEM);
    return data !== undefined 
           && data !== null 
           && current !== undefined
           && current !== null
           && data.user_name === JSON.parse(current).login;
  }

  logout = () => {
    this.setTokenSession(null);
    this.removeUser();
  }

  // Set token to all http request header, so you don't need to attach everytime
  setTokenSession = token => {
    if (token) {
      localStorage.setItem(LOCALSTORAGE.TOKEN_ITEM, token);
      this.setDefaultRequestsHeader();
      return;
    } 
    
    localStorage.removeItem(LOCALSTORAGE.TOKEN_ITEM);
    delete axios.defaults.headers.common["Authorization"];
  };

  // Set Authorization to header requests
  setDefaultRequestsHeader = () => {
    axios.defaults.headers.common["Authorization"] = "Bearer " 
    + localStorage.getItem(LOCALSTORAGE.TOKEN_ITEM);
  }

  // Save user to localstorage
  setUser = (user) => {
    localStorageService.setItem(LOCALSTORAGE.USER_ITEM, user);
  }
  // Remove user from localstorage
  removeUser = () => {
    localStorage.removeItem(LOCALSTORAGE.USER_ITEM);
  }
}

export default new JwtAuthService();
