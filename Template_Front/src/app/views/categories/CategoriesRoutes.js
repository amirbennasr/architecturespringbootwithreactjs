import { MatxLoadable } from "matx";
import { roles } from "../../app_roles/roles";



const ListForm = MatxLoadable({
  loader: () => import("./ListForm")
});

const categoriesRoutes = [
  {
    path: "/categories/list",
    component: ListForm,
    auth: roles.dashboard
  }
];

export default categoriesRoutes;
