import React, { Component } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { createCategory } from "../../redux/actions/CategoryAction";
import { updateCategory } from "../../redux/actions/CategoryAction";
import { getCategory} from "../../redux/actions/CategoryAction";
import { getCategories} from "../../redux/actions/CategoryAction";
import { deleteCategory } from "../../redux/actions/CategoryAction";

import { connect } from 'react-redux';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/primereact.css';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primeicons/primeicons.css';
export class ListForm extends Component {
  
  
   
     constructor(props) {
        super(props);
        
        this.state = {
            listCategory: [],
            CategoryDialog: false,
            deleteCategoryDialog: false,
            deleteCategorysDialog: false,
            category:{
                name:'',
                description:''
            },
            selectedCategorys: null,
            submitted: false,
            globalFilter: null
        };
        
        this.leftToolbarTemplate = this.leftToolbarTemplate.bind(this);
        this.rightToolbarTemplate = this.rightToolbarTemplate.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);

        this.openNew = this.openNew.bind(this);
        this.hideDialog = this.hideDialog.bind(this);
        this.saveCategory = this.saveCategory.bind(this);
        this.editCategory = this.editCategory.bind(this);
        this.confirmDeleteCategory = this.confirmDeleteCategory.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
        this.exportCSV = this.exportCSV.bind(this);
        this.confirmDeleteSelected = this.confirmDeleteSelected.bind(this);
        this.deleteSelectedCategorys = this.deleteSelectedCategorys.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.hideDeleteCategoryDialog = this.hideDeleteCategoryDialog.bind(this);
        this.hideDeleteCategorysDialog = this.hideDeleteCategorysDialog.bind(this);
    }

    componentWillMount() {
      console.log('------------ get All Categories -------------------');
    }

    componentWillRecieveProps({ listCategory }) {
        this.setState({ listCategory });
    }

    componentDidMount() {
      
      this.props.getCategories();
      console.log('[ComponentDidMount]: Items: ', this.state.listCategory);
    }

    openNew() {
        this.setState({
            category: this.state.category,
            submitted: false,
            CategoryDialog: true
        });
    }

    hideDialog() {
        this.setState({
            submitted: false,
            CategoryDialog: false
        });
    }

    hideDeleteCategoryDialog() {
        this.setState({ deleteCategoryDialog: false });
    }

    hideDeleteCategorysDialog() {
        this.setState({ deleteCategorysDialog: false });
    }

    saveCategory() {
        let state = { submitted: true };

        if (this.state.category.name.trim()) {
            if (this.state.category.id) {
                
                this.props.updateCategory(this.state.category)
        
                this.toast.show({ severity: 'success', summary: 'Successful', detail: 'Category Updated', life: 3000 });
            }
            else {
              this.props.createCategory(this.state.category)
              this.toast.show({ severity: 'success', summary: 'Successful', detail: 'Category Created', life: 3000 });
            }

            state = {
                ...state,
                getCategories:this.state.listCategory,
                CategoryDialog: false,
            };
        }

        this.setState(state);
    }

    editCategory(category) {
        console.log(category,'objet edit')
        this.props.getCategory(category.id)
        debugger
        this.setState({
            category: { ...category },
            CategoryDialog: true
        });
    }

    confirmDeleteCategory(category) {
        this.setState({
        category,
        deleteCategoryDialog: true,
        });
    }

    deleteCategory() {
      this.props.deleteCategory(this.state.category.id);
        this.setState({
            getCategories:!this.props.listCategory,
            deleteCategoryDialog: false,
        });
        this.toast.show({ severity: 'success', summary: 'Successful', detail: 'Category Deleted', life: 3000 });
    }

    exportCSV() {
        this.dt.exportCSV();
    }

    confirmDeleteSelected() {
        this.setState({ deleteCategorysDialog: true });
    }

    deleteSelectedCategorys() {
        this.props.deleteCategory(this.state.category.id);
        this.setState({
            deleteCategorysDialog: false,
            selectedCategorys: null
        });
        this.toast.show({ severity: 'success', summary: 'Successful', detail: 'Categorys Deleted', life: 3000 });
    }

  

    onInputChange(e, name) {
        const val = (e.target && e.target.value) || '';
        let category = {...this.state.category};
        category[`${name}`] = val;

        this.setState({ category });
    }

   
    leftToolbarTemplate() {
        return (
            <React.Fragment>
                <Button label="New" icon="pi pi-plus" className="p-button-success p-mr-2" onClick={this.openNew} />
                <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={this.confirmDeleteSelected} disabled={!this.state.selectedCategorys || !this.state.selectedCategorys.length} />
            </React.Fragment>
        )
    }

    rightToolbarTemplate() {
        return (
            <React.Fragment>
                <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="p-mr-2 p-d-inline-block" />
                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={this.exportCSV} />
            </React.Fragment>
        )
    }

    

    actionBodyTemplate(rowData) {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success p-mr-2" onClick={() => this.editCategory(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-warning" onClick={() => this.confirmDeleteCategory(rowData)} />
            </React.Fragment>
        );
    }

    render() {
        const header = (
            <div className="table-header">
                <h5 className="p-m-0">Manage Categories</h5>
                <span className="p-input-icon-left">
                    <i className="pi pi-search" />
                    <InputText type="search" onInput={(e) => this.setState({ globalFilter: e.target.value })} placeholder="Search..." />
                </span>
            </div>
        );
        const CategoryDialogFooter = (
            <React.Fragment>
                <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog} />
                <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={this.saveCategory} />
            </React.Fragment>
        );
        const deleteCategoryDialogFooter = (
            <React.Fragment>
                <Button label="No" icon="pi pi-times" className="p-button-text" onClick={this.hideDeleteCategoryDialog} />
                <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={this.deleteCategory} />
            </React.Fragment>
        );
        const deleteCategorysDialogFooter = (
            <React.Fragment>
                <Button label="No" icon="pi pi-times" className="p-button-text" onClick={this.hideDeleteCategorysDialog} />
                <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={this.deleteSelectedCategorys} />
            </React.Fragment>
        );

        return (
            <div className="datatable-crud-demo">
                <Toast ref={(el) => this.toast = el} />

                <div className="card">
                    <Toolbar className="p-mb-4" left={this.leftToolbarTemplate} right={this.rightToolbarTemplate}></Toolbar>

                    <DataTable ref={(el) => this.dt = el} value={this.props.listCategory} selection={this.state.selectedCategorys} onSelectionChange={(e) => this.setState({ selectedCategorys: e.value })}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} Categories"
                        globalFilter={this.state.globalFilter} 
                        header={header}>

                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="name" header="Name" sortable></Column>
                        <Column field="description" header="Description" sortable></Column>
                        <Column body={this.actionBodyTemplate}></Column>
                    </DataTable>
                </div>

                <Dialog visible={this.state.CategoryDialog} style={{ width: '450px' }} header="Category Details" modal className="p-fluid" footer={CategoryDialogFooter} onHide={this.hideDialog}>
                <div className="p-field">
                        <label htmlFor="name">Name</label>
                        <InputText id="name" value={this.state.category.name} onChange={(e) => this.onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': this.state.submitted && !this.state.category.name })} />
                        {this.state.submitted && !this.state.category.name && <small className="p-invalid">Name is required.</small>}
                    </div>
                    <div className="p-field">
                        <label htmlFor="description">Description</label>
                        <InputTextarea id="description" value={this.state.category.description} onChange={(e) => this.onInputChange(e, 'description')} required rows={3} cols={20} />
                    </div>
                   
                 
                </Dialog>

                <Dialog visible={this.state.deleteCategoryDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCategoryDialogFooter} onHide={this.hideDeleteCategoryDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem'}} />
                        {this.state.category && <span>Are you sure you want to delete <b>{this.state.category.name}</b>?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.deleteCategorysDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCategorysDialogFooter} onHide={this.hideDeleteCategorysDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem'}} />
                        {this.state.category && <span>Are you sure you want to delete the selected categories?</span>}
                    </div>
                </Dialog>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {

  return {
    listCategory: state.category.categoryList,
    
  }
  
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: () => dispatch(getCategories()),
    getCategory: index=> dispatch(getCategory(index)),
    deleteCategory: index =>dispatch(deleteCategory(index)),
    createCategory: category =>dispatch(createCategory(category)),
    updateCategory: category =>dispatch(updateCategory(category))

  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ListForm);