import { MatxLoadable } from "matx";
import { roles } from "../../app_roles/roles";

const Analytics = MatxLoadable({
  loader: () => import("./Analytics")
})

const dashboardRoutes = [
  {
    path: "/dashboard/analytics",
    component: Analytics,
    auth: roles.dashboard
  }
];

export default dashboardRoutes;
