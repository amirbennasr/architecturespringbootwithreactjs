import SignUp from "./SignUp";
import SignIn from "./SignIn";
import NotFound from "./NotFound";
import NoRight from "./NoRight";
import ForgotPassword from "./ForgotPassword";

import { LOCAL_PATHS } from '../../configuration/LocalPaths';

const settings = {
  activeLayout: "layout1",
  layout1Settings: {
    topbar: {
      show: false
    },
    leftSidebar: {
      show: false,
      mode: "close"
    }
  },
  layout2Settings: {
    mode: "full",
    topbar: {
      show: false
    },
    navbar: { show: false }
  },
  secondarySidebar: { show: false },
  footer: { show: false }
};

const sessionRoutes = [
  {
    path: LOCAL_PATHS.SING_UP,
    component: SignUp,
    settings
  },
  {
    path: LOCAL_PATHS.SINGIN,
    component: SignIn,
    settings
  },
  {
    path: LOCAL_PATHS.FORGET_PASSWORD,
    component: ForgotPassword,
    settings
  },
  {
    path: LOCAL_PATHS.NOT_FOUND,
    component: NotFound,
    settings
  },
  {
    path: LOCAL_PATHS.NO_RIGHT,
    component: NoRight,
    settings
  }
];

export default sessionRoutes;
